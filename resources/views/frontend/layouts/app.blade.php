<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/favicon.png')}}">
        <link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>@yield('title') | {{ config('app.name') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="@yield('meta_description')">
        <meta name="keyword" content="@yield('meta_keyword')">
        @include('frontend.includes.meta')
        <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
        <link rel="icon" type="image/ico" href="{{asset('img/favicon.png')}}" />
        <script
            src="https://code.jquery.com/jquery-3.7.1.js"
            integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4="
            crossorigin="anonymous"></script>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @vite(['resources/css/app-frontend.css'])
        @vite(['resources/js/app-frontend.js'])

        @livewireStyles

        @stack('after-styles')

        <x-google-analytics />




        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;600;700&display=swap" rel="stylesheet">


        <link href="{{asset('fonts/icomoon/style.css')}}" rel="stylesheet">
        <link rel="stylesheet"  href="{{asset('fonts/flaticon/font/flaticon.css')}}" >

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

        <link  rel="stylesheet"  href="{{asset('css/tiny-slider.css')}}">
        <link rel="stylesheet"  href="{{asset('css/aos.css')}}">
        <link rel="stylesheet" href="{{asset('css/glightbox.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">


        <link rel="stylesheet" href="css/flatpickr.min.css">


        <title>Блог &mdash; Free Bootstrap 5 Website Template by Untree.co</title>
    </head>


<body>
    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close">
                <span class="icofont-close js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>

    <nav class="site-nav">
        <div class="container">
            <div class="menu-bg-wrap">

                <div class="site-navigation">
                    <div class="row g-0 align-items-center">
                        <div class="col-2">
                            <a href="#" class="logo m-0 float-start">Блог<span class="text-primary">.</span></a>
                        </div>
                        <div class="col-8 text-center">
                            <form action="#" class="search-form d-inline-block d-lg-none">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="bi-search"></span>
                            </form>

                            <ul class="js-clone-nav d-none d-lg-inline-block text-start site-menu mx-auto">
                                <li class="active"><a href="/">Home</a></li>
                                <li class="has-children">
                                    <a href="#">Pages</a>
                                    <ul  class="dropdown">
                                        <li><a href="/posts">Блог</a></li>
                                        <li><a href="/page/about">О нас</a></li>
                                    <!--    <li><a href="single.html">Blog Single</a></li>
                                        <li><a href="category.html">Category</a></li>
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="contact.html">Contact Us</a></li>
                                        <li><a href="#">Menu One</a></li>
                                        <li><a href="#">Menu Two</a></li>
                                        <li class="has-children">
                                            <a href="#">Dropdown</a>
                                            <ul class="dropdown">
                                                <li><a href="#">Sub Menu One</a></li>
                                                <li><a href="#">Sub Menu Two</a></li>
                                                <li><a href="#">Sub Menu Three</a></li>
                                            </ul>
                                        </li> !-->
                                    </ul>
                                </li>
                                <li><a href="/categories">Категории</a></li>
                              <!--  <li><a href="category.html">Business</a></li>
                                <li><a href="category.html">Politics</a></li> !-->
                            </ul>
                        </div>
                        <div class="col-2 text-end">
                            <a href="#" class="burger ms-auto float-end site-menu-toggle js-menu-toggle d-inline-block d-lg-none light">
                                <span></span>
                            </a>
                            <form action="{{ url('/posts') }}" method="GET" class="search-form d-none d-lg-inline-block">
                                @csrf
                                <input name="query" type="text" class="form-control" placeholder="Search...">
                                <span class="bi-search"></span>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </nav>

    <main>
        @yield('content')


    </main>
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="widget">
                        <h3 class="mb-4">About</h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    </div> <!-- /.widget -->
                    <div class="widget">
                        <h3>Social</h3>
                        <ul class="list-unstyled social">
                            <li><a href="#"><span class="icon-instagram"></span></a></li>
                            <li><a href="#"><span class="icon-twitter"></span></a></li>
                            <li><a href="#"><span class="icon-facebook"></span></a></li>
                            <li><a href="#"><span class="icon-linkedin"></span></a></li>
                            <li><a href="#"><span class="icon-pinterest"></span></a></li>
                            <li><a href="#"><span class="icon-dribbble"></span></a></li>
                        </ul>
                    </div> <!-- /.widget -->
                </div> <!-- /.col-lg-4 -->
                <div class="col-lg-4 ps-lg-5">
                    <div class="widget">
                        <h3 class="mb-4">Company</h3>
                        <ul class="list-unstyled float-start links">
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Vision</a></li>
                            <li><a href="#">Mission</a></li>
                            <li><a href="#">Terms</a></li>
                            <li><a href="#">Privacy</a></li>
                        </ul>
                        <ul class="list-unstyled float-start links">
                            <li><a href="#">Partners</a></li>
                            <li><a href="#">Business</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Creative</a></li>
                        </ul>
                    </div> <!-- /.widget -->
                </div> <!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    @php

                        @endphp
                    @if($recent)
                    <div class="widget">
                        <h3 class="mb-4">Recent Post Entry</h3>
                        <div class="post-entry-footer">
                            <ul>
                                @foreach ($recent as $singular)
                                    @php
                                        $details_url = route("frontend.posts.show",[encode_id($singular->id), $singular->slug]);
                                       $created_by = $singular->created_by_alias ? $singular->created_by_alias : $singular->created_by_name;
                                       $date = $singular->published_at->format('Y-m-d');
                                       $author = \Illuminate\Support\Facades\DB::table('users')->where('id', $singular->$created_by)->first();
                                       $avatar = $author ? $author->avatar : 'img/default-avatar.jpg';

                                    @endphp
                                    <li>
                                        <a href="">
                                            <img src="{{$singular->featured_image}}" alt="Image placeholder" class="me-4 rounded">
                                            <div class="text">
                                                <h4>{{$singular->name}}</h4>
                                                <div class="post-meta">
                                                    <span class="mr-2">{{$date}}</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>

                            @endforeach
                            </ul>
                        </div>


                    </div> <!-- /.widget -->
                    @endif
                </div> <!-- /.col-lg-4 -->
            </div> <!-- /.row -->

            <div class="row mt-5">
                <div class="col-12 text-center">
                    <!--
                        **==========
                        NOTE:
                        Please don't remove this copyright link unless you buy the license here https://untree.co/license/
                        **==========
                      -->

                    <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script>.
                        All Rights Reserved. &mdash; Designed with love by <a href="https://gitlab.com/webklassik/studio"> Webclassik</a>  Distributed by <a href="#">Вебклассик</a> <!-- License information: https://untree.co/license/ -->
                    </p>
                </div>
            </div>
        </div> <!-- /.container -->
    </footer> <!-- /.site-footer -->

    <!-- Preloader -->
    <!-- Preloader -->
    <div id="overlayer"></div>
    <div class="loader">
        <div class="spinner-border text-primary" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>
    </div>


    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/tiny-slider.js')}}"></script>

    <script src="{{asset('js/flatpickr.min.js')}}"></script>


    <script src="{{asset('js/aos.js')}}" ></script>
    <script src="{{asset('js/glightbox.min.js')}}"></script>
    <script src="{{asset('js/navbar.js')}}"></script>
    <script src="{{asset('js/counter.js')}}"></script>
    <script src="{{asset('js/custom.js')}}"></script>
    <script type="text/javascript">

        $(document).ready(function(){


            $('.has-children').on("click", function(){
                setTimeout(function(){
                    console.log($('.collapse'));
                    $('.collapse').css("visibility", 'visible');
                }, 500);

            });
        });

    </script>



    <!-- Scripts -->
    @livewireScripts
    @stack('after-scripts')
</body>

</html>
