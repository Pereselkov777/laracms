

@extends('frontend.layouts.app')
@php
  $meta_title =   $model->meta_name ? $model->meta_name : '';
  $meta_description = $model->meta_description ? $model->meta_description : '';
  $meta_keyword = $model->meta_keyword ? $model->meta_keyword : '';
@endphp
@section('title')
    {{$meta_title}}
@stop
@section('meta_description',  $meta_description)
@section('meta_keyword', $meta_keyword)




@section('content')

<section class="bg-gray-100 text-gray-600 py-10 sm:py-20">
    <div class="container mx-auto flex px-5 items-center justify-center flex-col">
        <div class="text-center lg:w-2/3 w-full">
            <p class="mb-8 leading-relaxed">

            </p>
            <h1 class="text-3xl sm:text-4xl mb-4 font-medium text-gray-800">

            </h1>
            <p class="mb-8 leading-relaxed">
                {{$model->description}}
            </p>

            @include('frontend.includes.messages')
        </div>
    </div>
</section>

<!--<section class="bg-white text-gray-600 p-6 sm:p-20">
    <div class="grid grid-cols-1 sm:grid-cols-2 gap-6">
        <div>
            Content area.
        </div>
    </div>
</section> --!>

@endsection
