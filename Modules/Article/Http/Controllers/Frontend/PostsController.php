<?php

namespace Modules\Article\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Modules\Article\Events\PostViewed;
use Modules\Category\Models\Category;

class PostsController extends Controller
{
    public $module_title;

    public $module_name;

    public $module_path;

    public $module_icon;

    public $module_model;

    public function __construct()
    {
        // Page Title
        $this->module_title = 'Posts';

        // module name
        $this->module_name = 'posts';

        // directory path of the module
        $this->module_path = 'posts';

        // module icon
        $this->module_icon = 'fas fa-file-alt';

        // module model name, path
        $this->module_model = "Modules\Article\Models\Post";
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);
        $categories = Category::all();


        $module_action = 'List';
        if (isset($_GET["query"])) {

            $str = strip_tags($_GET['query']);

            $$module_name = $module_model::latest()->with(['category', 'tags', 'comments'])
                ->where('name','LIKE','%' .  $str. '%')
                ->orWhere('content','LIKE','%' .  $str. '%')
           ->orWhere('intro','LIKE', '%' .  $str. '%')
               ->paginate();
          //  ->toSql();
          //      dd($$module_name);
            //    exit;
        } else {
            $$module_name = $module_model::latest()->with(['category', 'tags', 'comments'])->paginate();

           // dd($$module_name);

        }

        /*foreach ($$module_name as $$module_name_singular){
            $now = Carbon::now()->setTimezone(env('TIMEZONE'));
            $difference = ($$module_name_singular->published_at->diff($now)->days);
            if ($difference < 1) {
                $recent[] = $$module_name_singular;
            }


        } */

        return view(
            "article::frontend.{$module_path}.index",
            compact('module_title', 'module_name',
                "{$module_name}", 'module_icon',
                'module_action', 'module_name_singular',
                'categories')
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($hashid)
    {
        $id = decode_id($hashid);

        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'Show';

        $meta_page_type = 'article';

        $$module_name_singular = $module_model::findOrFail($id);

        event(new PostViewed($$module_name_singular));


        return view(
            "article::frontend.{$module_name}.show",
            compact('module_title', 'module_name', 'module_icon', 'module_action', 'module_name_singular', "{$module_name_singular}", 'meta_page_type')
        );
    }
}
