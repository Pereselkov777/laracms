@extends('frontend.layouts.app')

@section('title') {{ __("Posts") }}

@endsection
@section('meta_description',  'CMS like modular starter application project built with latest Laravel.')
@section('meta_keyword', 'Cms, Laravel, Blog')

@section('content')


    <section class="section">
        <div class="container">

            <div class="row mb-4">
                <div class="col-sm-6">
                    <h2 class="posts-entry-title">Посты</h2>
                </div>
                <div class="col-sm-6 text-sm-end"><a href="/categories" class="read-more">View All</a></div>
            </div>

            <div class="row">
                @foreach ($$module_name as $$module_name_singular)
                    @php
                        $details_url = route("frontend.$module_name.show",[encode_id($$module_name_singular->id), $$module_name_singular->slug]);
                        $created_by = $$module_name_singular->created_by_alias ? $$module_name_singular->created_by_alias : $$module_name_singular->created_by_name;
                        $date = $$module_name_singular->published_at->format('Y-m-d');
                        $author = \Illuminate\Support\Facades\DB::table('users')->where('id', $$module_name_singular->$created_by)->first();
                        $avatar = $author ? $author->avatar : 'img/default-avatar.jpg';
                    @endphp

                        <div class="col-lg-4 mb-4">
                            <div class="post-entry-alt">
                                <a href="{{$details_url}}" class="img-link"><img src="{{$$module_name_singular->featured_image}}" alt="Image" class="img-fluid"></a>
                                <div class="excerpt">


                                    <h2><a href="{{$details_url}}">{{$$module_name_singular->slug}}</a></h2>
                                    <div class="post-meta align-items-center text-left clearfix">

                                        <figure class="author-figure mb-0 me-3 float-start"><img src="{{asset('img/default-avatar.jpg')}}" alt="Image" class="img-fluid"></figure>
                                        <span class="d-inline-block mt-1">By <a href="#">{{$created_by}}</a></span>
                                        <span>&nbsp;-&nbsp; {{$date}}</span>
                                    </div>

                                    <p> {{$$module_name_singular->intro}}</p>
                                    <p><a href="{{$details_url}}" class="read-more">Continue Reading</a></p>
                                </div>
                            </div>
                        </div>



                @endforeach
                    {{$$module_name->links()}}
            </div>

        </div>
    </section>

@endsection
